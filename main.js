$(document).ready(function() {

    $('.alert').on('click', function(){
        alert('Check function alert')
    })  
    $('.slider').slick({
        autoplay: true,
        autoplaySpeed:3000,
        mobileFirst: true,
        arrow:true,
        prevArrow: '<button class=" Prev button button--hov button--bg_color_gray button--border_none"><img src="img/arrow__left.png" alt=""></button>',
        nextArrow: '<button class=" Next button button--hov button--bg_color_gray button--border_none"><img src="img/right__arrow.png" alt=""></button>',
        appendArrows: $('.prevnext'),
        centerMode: true,
        fade: true
      })
    
    $.validator.addMethod("customemail", 
    function(value, element) {
        return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
    }, 
    "Sorry, I've enabled very strict email validation"
    );
    $(".forms").validate({
        rules: {
        email: {
        required:  {
            depends:function(){
            $(this).val($.trim($(this).val()));
            return true;
            }},
        customemail: true
        }}});
})